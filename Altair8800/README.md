# Altair 8800

Notes, programs, and information on using the
[Altair 8800](https://en.wikipedia.org/wiki/Altair_8800) computer.


## Web Resources

* [Program the Altair 8800 emulator](https://learn.microsoft.com/en-us/training/modules/altair-azure-sphere-program-emulator/)
* [How to Write a Program for a 1975 ALTAIR 8800 Computer](https://blog.devgenius.io/how-to-write-a-program-for-altair-8800-computer-3a4583fe601e)
* [An Intel 8080 assembler and online simulator](https://eli.thegreenplace.net/2020/an-intel-8080-assembler-and-online-simulator/)
