# Set Up Linux Workstation to Connect to Altair Clone

## Debian Packages

* minicom


## Resources

* [Connecting the Altair Clone](https://ubuntourist.codeberg.page/Altair-8800/linux.html#connecting-the-altair-clone)
