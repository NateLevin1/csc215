# Part 1. Introduction

Reading the first paragraphs of the introduction, I was struck again by just
what a sweet spot the Altair 8800 holds in the history of computing and why
this machine makes such a good object of study for an introductory computer
organization course:

* It occurred *just* as advances in [large scale
  integration](https://en.wikipedia.org/wiki/Integrated_circuit#LSI) made it
  possible to put an entire general purpose computer on a single chip.
* It played such an important role in the history of personal computing that
  it provides rich opportunity for interdisciplinary excursions into the
  history of technology.
* It is big enough to have the essential components of a modern computer system,
  yet small enough to fit in the head of the learner.
* As the manual states, "Users of the ALTAIR 8800 include persons with a strong
  electronics background and little or no computer experience and persons with
  considerable programming experience and little or no electronics background.
  Accordingly, this manual has been prepared with all types of users in mind."
  This is what makes it a good textbook for our course.

## The Delivery Plan

Get students to work together in groups to present the sections on logic gates
and number systems while I focus on learning to program the machine better.

Student presentations should greatly expand the brief discussions of:

* The history of the
  [Intel 8080 Microprocessor](https://en.wikipedia.org/wiki/Intel_8080) in the
  context of development of
  [integrated circuits](https://en.wikipedia.org/wiki/Integrated_circuit).
* George Boole and **Boolean algebra**.
* How [transistors](https://en.wikipedia.org/wiki/Transistor) are used to
  create [logic gates](https://en.wikipedia.org/wiki/Logic_gate).
* [Flip-flops](https://en.wikipedia.org/wiki/Flip-flop_(electronics)) and
  computer memory.
* In discussion of
  [number systems](https://en.wikipedia.org/wiki/Numeral_system), the
  [decimal](https://en.wikipedia.org/wiki/Decimal),
  [binary](https://en.wikipedia.org/wiki/Binary_number),
  [octal](https://en.wikipedia.org/wiki/Octal), and
  [hexadecimal](https://en.wikipedia.org/wiki/Hexadecimal) systems should be
  thoroughly presented. 
