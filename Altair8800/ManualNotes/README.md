# Altair 8800 Manual Notes

Thanks to the labor of love [Kevin Cole](https://codeberg.org/ubuntourist) put
into porting, editing, and augmenting a new version of the
[Altair 8800 Operator's Manual](https://ubuntourist.codeberg.page/Altair-8800/),
it can now serve alone as a textbook for an introductory computer organization
course.

That's just how I'm using it, and I'll post notes and reflections to it here.
