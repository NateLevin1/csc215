
   STEP  MNEMONIC       BIT PATTERN    OCTAL EQUIVALENT
   ---- -------------   -----------    ----------------
     0     LDA          00 111 010          072
     1     (address)    00 000 000          000
     2     (address)    00 000 000          000
     3     STA          00 110 010          062
     4     (address)    10 000 000          200
     5     (address)    00 000 000          000
     6     LDA          00 111 010          072
     7     (address)    00 000 001          001
     8     (address)    00 000 000          000
     9     STA          00 110 010          062
    10     (address)    10 000 001          201
    11     (address)    00 000 000          000                    
    12     LDA          00 111 010          072
    13     (address)    00 000 010          002
    14     (address)    00 000 000          000
    15     STA          00 110 010          062
    16     (address)    10 000 010          202
    17     (address)    00 000 000          000
    18     JMP          11 000 011          303
    19     (address)    00 000 000          000
    20     (address)    00 000 000          000
