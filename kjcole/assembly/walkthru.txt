## 2020.11.15 KJC

This is taken from the multiply-trace.ods spreadsheet, 0F * 0F = E1

Accumulator
================================================
  0000 1111    0F
  0001 1110    1E
  0011 1100    3C
  0111 1000    78
  1111 0000    F0
  1110 0000    E0   *** Carry set *** (Row 28)
================================================

Meanwhile, D (E is zeroed and unused)
=======================================
  0000 1111    00
=======================================

Meanwhile, H (L is zeroed and unused)
============================================================
  0000 0000    00   *** Until carry set above *** (Row 30)
============================================================

Net effect ?
============================
  H + D -> (00 + 0F) -> 0F
  H + H -> (0F + 0F) -> 1E
  H + D -> (1E + 0F) -> 2D
  H + H -> (1E + 1E) -> 5A
  H + D -> (5A + 0F) -> 69
  H + H -> (69 + 69) -> D2
  H + D -> (69 + 0F) -> E1
============================

So... It's alternating between adding in a 0F and doubling itself.

----

## 2020.11.19


accumulator = 0x15    # 0001 0101
register_de = 0x0A00  # 0000 1010  0000 0000
register_hl = 0x0000  # 0000 0000  0000 0000
for register_b in range(7, -1, -1):  # Loop 8 times
    register_hl += register_hl       # Double multiplicand
    accumulator *= 2                 # Check next bit (from high to low)
    if accumulator & 0x100:          # If carry bit set...
        register_hl += register_de   # ...add in multiplier
	
