# Computer Organization

A collection of Assembly, C, Python, Linux, and other resources for use in a
[dual-enrolled](https://en.wikipedia.org/wiki/Dual_enrollment)
[Computer Systems](https://courses.vccs.edu/courses/CSC215-ComputerSystems)
course offered at the  
[Arlington Career Center](https://careercenter.apsva.us/) through
[Northern Virginia Community College](https://www.nvcc.edu/).
