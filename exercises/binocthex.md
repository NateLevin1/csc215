# Binary, Octal and Hexidecimal Conversion Practice

1. Convert `0xAC3B` to octal.
1. Convert `0o317245` to hexidecimal. 
1. Convert `3758` to binary, octal, and hex. 
1. Convert `3903276` to hex, octal, and binary. 
1. Convert `0o57321` to hex.
