# DOS

Notes, programs, and resources about to use the
[DOS](https://en.wikipedia.org/wiki/DOS) version of a
[DOS](https://en.wikipedia.org/wiki/Disk_operating_system)


## Resources

* [How to copy files between Linux and FreeDOS](http://www.freedos.org/books/get-started/june24-guestmount-freedos.html)

### FreeDOS

* [A gentle introduction to FreeDOS](https://opensource.com/article/18/4/gentle-introduction-freedos)
* [How to run DOS programs in Linux](https://opensource.com/article/17/10/run-dos-applications-linux)
* [Running DOS on the Raspberry Pi](https://opensource.com/article/18/3/can-you-run-dos-raspberry-pi)
* [Set and use environment variables in FreeDOS](https://opensource.com/article/21/6/freedos-environment-variables)

### MS-DOS

* [PCjs Machines Microsoft MS-DOS 2.00](https://www.pcjs.org/software/pcx86/sys/dos/microsoft/2.00/)
