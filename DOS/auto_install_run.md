# Auto Installation and Running of FreeDOS

Thanks to [R.s. Doiel](https://rsdoiel.github.io/blog/2021/11/27/FreeDOS-1.3rc4-with-Qemu.html),
I have an installation script that will install a FreeDOS instance using
[qemu](https://www.qemu.org/) and then launch the instance once installed.

The script has been added to this repo.
